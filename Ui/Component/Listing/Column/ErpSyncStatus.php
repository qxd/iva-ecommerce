<?php

namespace Qxd\ERP\Ui\Component\Listing\Column;

/**
 * Created by PhpStorm.
 * User: jgutierrez@qxdev.com
 * Date: 09/01/19
 * Time: 01:17 PM
 */

use \Magento\Sales\Api\OrderRepositoryInterface;
use \Magento\Framework\View\Element\UiComponent\ContextInterface;
use \Magento\Framework\View\Element\UiComponentFactory;
use \Magento\Ui\Component\Listing\Columns\Column;
use \Magento\Framework\Api\SearchCriteriaBuilder;

class ErpSyncStatus extends Column
{
    protected $_orderRepository;
    protected $_searchCriteria;

    public function __construct(ContextInterface $context, UiComponentFactory $uiComponentFactory, OrderRepositoryInterface $orderRepository, SearchCriteriaBuilder $criteria, array $components = [], array $data = [])
    {
        $this->_orderRepository = $orderRepository;
        $this->_searchCriteria = $criteria;
        parent::__construct($context, $uiComponentFactory, $components, $data);
    }

    public function prepareDataSource(array $dataSource)
    {
        if (isset($dataSource['data']['items'])) {
            foreach ($dataSource['data']['items'] as & $item) {

                $order = $this->_orderRepository->get($item["entity_id"]);
                $status = $order->getData("erp_sync_status");

                if ($status) {
                    $message = 'Sent';
                } else {
                    $message = 'Waiting';
                }

                $item[$this->getData('name')] = $message;
            }
        }

        return $dataSource;
    }
}