<?php

namespace Qxd\ERP\Cron;

/**
 * Created by PhpStorm.
 * User: jgutierrez@qxdev.com
 * Date: 10/01/19
 * Time: 12:32 PM
 */

use DateTime;
use Magento\Framework\Exception\LocalizedException;

class Orders
{
    /**
     * API url to send the orders
     */
    const REQUEST_API_URL = 'api/magento2/queue-orders';

    /**
     * @var \Qxd\ERP\Logger\ERPLogger
     */
    protected $_logger;

    /**
     * @var \Qxd\ERP\Helper\Data
     */
    protected $_erpHelper;

    /**
     * @var \Magento\Sales\Model\ResourceModel\Order\CollectionFactory
     */
    protected $_orderCollectionFactory;

    /**
     * @var \Magento\Framework\Stdlib\DateTime\TimezoneInterface
     */
    protected $_timezone;

    /**
     * @var \Magento\Framework\App\State
     */
    protected $_state;

    /**
     * Orders constructor.
     * @param \Qxd\ERP\Logger\ERPLogger $logger
     * @param \Qxd\ERP\Helper\Data $erpHelper
     * @param \Magento\Sales\Model\ResourceModel\Order\CollectionFactory $orderCollectionFactory
     * @param \Magento\Framework\Stdlib\DateTime\TimezoneInterface $timezone
     * @param \Magento\Framework\App\State $state
     */
    public function __construct(
        \Qxd\ERP\Logger\ERPLogger $logger,
        \Qxd\ERP\Helper\Data $erpHelper,
        \Magento\Sales\Model\ResourceModel\Order\CollectionFactory $orderCollectionFactory,
        \Magento\Framework\Stdlib\DateTime\TimezoneInterface $timezone,
        \Magento\Framework\App\State $state
    )
    {
        $this->_logger = $logger;
        $this->_erpHelper = $erpHelper;
        $this->_orderCollectionFactory = $orderCollectionFactory;
        $this->_timezone = $timezone;
        $this->_state = $state;
    }

    /**
     * Execute cronjob
     */
    public function execute()
    {
        $this->_logger->info(strtoupper('------------------------------------------------------------------------'));
        $this->_logger->info(strtoupper('Running QXD - ERP cronjob.'));

        try {
            $this->_logger->info(strtoupper('Setting area code.'));
            $this->_state->setAreaCode(\Magento\Framework\App\Area::AREA_CRONTAB);
            $this->_logger->info(strtoupper('Area code was set: ' . $this->_state->getAreaCode() . "."));
        } catch (LocalizedException $e) {
            $this->_logger->info(strtoupper('Something happening setting area code: ' . $e->getMessage()));
        }

        if ($this->_erpHelper->getEnable()) {

            $this->_logger->info(strtoupper('Starting ERP Orders synchronization.'));

            $orderStatusFilter = $this->_erpHelper->getOrderStatusConfig();
            $orderIdLimit = $this->_erpHelper->getOrderIdConfig();

            $this->_logger->info(strtoupper('Fetching orders with status ' . $orderStatusFilter . ' and ERP sync status on waiting.'));

            $ordersCollection = $this->_orderCollectionFactory
                ->create()
                ->addAttributeToSelect('*')
                ->addFieldToFilter('erp_sync_status', '0')
                ->addFieldToFilter('status', $orderStatusFilter);

            if(!empty($orderIdLimit)){
                $this->_logger->info(strtoupper('Order id start point: #' . $orderIdLimit));
                $ordersCollection->addFieldToFilter('entity_id', ['gteq' => $orderIdLimit]);
            }
            
            if ($ordersCollection->getTotalCount()) {
                if ($ordersCollection->getTotalCount() > 1)
                    $this->_logger->info(strtoupper($ordersCollection->getTotalCount() . ' orders were found to be synchronized.'));
                else
                    $this->_logger->info(strtoupper($ordersCollection->getTotalCount() . ' order was found to be synchronized.'));

                $this->_logger->info(strtoupper('Preparing request data object.'));

                $postDataArray = array();
                foreach ($ordersCollection as $order) {
                    try {
                        if(!empty($orderRequestArray = $this->_erpHelper->prepareOrderRequestData($order,0))){
                            $postDataArray[] = $orderRequestArray;
                        } else {
                            $this->_logger->info(strtoupper('------------------------------------------------------------------------'));
                            exit;
                        }
                    } catch (\Exception $e) {
                        $this->_logger->info(strtoupper('There was a problem processing the order, ' . $e->getMessage()));
                        $this->_logger->info(strtoupper('------------------------------------------------------------------------'));
                        exit;
                    }
                }

                $this->_logger->info(strtoupper('Request data object ready.'));

                $this->_logger->info(strtoupper('Sending request to ERP API.'));

                $responseData = $this->_erpHelper->sendRequest($postDataArray, self::REQUEST_API_URL);

                $this->_logger->info(strtoupper('Request has been sent.'));

                $this->_logger->info(strtoupper('Got response from ERP:'));

                $responseObject = (Object)json_decode($responseData);
                if (isset($responseObject->message)) {
                    $this->_logger->info(strtoupper($responseObject->message));
                } else {
                    $this->_logger->error(strtoupper('Something wrong happened, not seeing response message.'));
                    $this->_logger->info(strtoupper('------------------------------------------------------------------------'));
                    exit;
                }

                $this->_logger->info(strtoupper('Updating orders ERP Sync Status to sent.'));

                foreach ($ordersCollection as $order) {
                    try {
                        $this->_logger->info(strtoupper('      Updating order #' . $order->getIncrementId() . '.'));
                        $this->_logger->info(strtoupper('      ' . $responseObject->output->{$order->getId()}));
                        $dateTime = new DateTime("now");
                        $order->setErpSyncLog(json_encode(array($dateTime->format('Y-m-d H:i:s') . ': ' . $responseObject->output->{$order->getId()})));
                        $order->setErpSyncStatus(1);
                        $order->save();
                    } catch (\Exception $e) {
                        $this->_logger->error(strtoupper('There was a problem processing the order, ' . $e->getMessage()));
                        $this->_logger->info(strtoupper('------------------------------------------------------------------------'));
                        exit;
                    }
                }
            } else {
                $this->_logger->info(strtoupper('No orders found.'));
            }

            $this->_logger->info(strtoupper('Process finished.'));
            $this->_logger->info(strtoupper('------------------------------------------------------------------------'));

        } else {
            $this->_logger->info(strtoupper('QXD - ERP is disabled.'));
            $this->_logger->info(strtoupper('------------------------------------------------------------------------'));
        }
    }
}