<?php

namespace Qxd\ERP\Model\Config\Source;

/**
 * Created by PhpStorm.
 * User: jgutierrez@qxdev.com
 * Date: 09/01/19
 * Time: 04:20 PM
 */

class OrderId implements \Magento\Framework\Option\ArrayInterface
{
    /**
     * @var \Magento\Sales\Model\ResourceModel\Order\CollectionFactory
     */
    protected $_orderCollectionFactory;

    /**
     * OrderId constructor.
     * @param \Magento\Sales\Model\ResourceModel\Order\CollectionFactory $orderCollectionFactory
     */
    public function __construct(
        \Magento\Sales\Model\ResourceModel\Order\CollectionFactory $orderCollectionFactory
    )
    {
        $this->_orderCollectionFactory = $orderCollectionFactory;
    }

    /**
     * Options getter
     *
     * @return array
     */
    public function toOptionArray()
    {
        $orders = $this->_orderCollectionFactory
            ->create()
            ->addAttributeToSelect('entity_id')
            ->addAttributeToSelect('increment_id')
            ->addOrder('entity_id','DESC');

        $result = array();
        foreach ($orders as $order){
            $result[$order->getIncrementId()]['value'] = $order->getEntityId();
            $result[$order->getIncrementId()]['label'] = $order->getEntityId() . '  - #' . $order->getIncrementId();
        }
        return array_values($result);
    }
}
