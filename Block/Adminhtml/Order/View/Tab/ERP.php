<?php

namespace Qxd\ERP\Block\Adminhtml\Order\View\Tab;

/**
 * Created by PhpStorm.
 * User: jgutierrez@qxdev.com
 * Date: 09/01/19
 * Time: 02:00 PM
 */
class ERP extends \Magento\Backend\Block\Template implements \Magento\Backend\Block\Widget\Tab\TabInterface
{

    /**
     * API url to send the orders
     */
    const REQUEST_API_URL = 'api/magento2/status-order';

    /**
     * Template
     *
     * @var string
     */
    protected $_template = 'order/view/tab/erp.phtml';

    /**
     * Core registry
     *
     * @var \Magento\Framework\Registry
     */
    protected $coreRegistry = null;

    /**
     * @var \Qxd\ERP\Helper\Data
     */
    protected $_erpHelper;

    /**
     * @var \Qxd\ERP\Logger\ERPLogger
     */
    protected $_erpLogger;

    /**
     * @var null
     */
    protected $_queryResponse;

    /**
     * ERP constructor.
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param \Qxd\ERP\Helper\Data $erpHelper
     * @param \Qxd\ERP\Logger\ERPLogger $erpLogger
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Framework\Registry $registry,
        \Qxd\ERP\Helper\Data $erpHelper,
        \Qxd\ERP\Logger\ERPLogger $erpLogger,
        array $data = []
    )
    {
        $this->coreRegistry = $registry;
        $this->_erpHelper = $erpHelper;
        $this->_erpLogger = $erpLogger;
        $this->_queryResponse = NULL;
        parent::__construct($context, $data);
    }

    /**
     * Retrieve order model instance
     *
     * @return \Magento\Sales\Model\Order
     */
    public function getOrder()
    {
        return $this->coreRegistry->registry('current_order');
    }

    /**
     * {@inheritdoc}
     */
    public function getTabLabel()
    {
        return __('QXD - ERP');
    }

    /**
     * {@inheritdoc}
     */
    public function getTabTitle()
    {
        return __('QXD - ERP');
    }

    /**
     * {@inheritdoc}
     */
    public function canShowTab()
    {
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function isHidden()
    {
        return false;
    }

    /**
     * Get Tab Class
     *
     * @return string
     */
    public function getTabClass()
    {
        return 'ajax only';
    }

    /**
     * Get Class
     *
     * @return string
     */
    public function getClass()
    {
        return $this->getTabClass();
    }

    /**
     * Get Tab Url
     *
     * @return string
     */
    public function getTabUrl()
    {
        return $this->getUrl('erp/*/orderTab', ['_current' => true]);
    }

    /**
     * @param $type
     * @return \Magento\Framework\Phrase|string
     */
    public function getTreasuryData($type)
    {
        if (!$this->_queryResponse) {
            $this->_queryResponse = $this->_erpHelper->sendRequest(array(
                'order_id' => $this->getOrder()->getId(),
                'store_hash' => $this->_erpHelper->getCleanBaseUrl()
            ), self::REQUEST_API_URL);
        }

        $queryStatusObject = (Object)json_decode($this->_queryResponse);
        if (!isset($queryStatusObject->message) || !isset($queryStatusObject->output)) {
            $this->_erpLogger->info(strtoupper('------------------------------------------------------------------------'));
            $this->_erpLogger->info(strtoupper('Something wrong happened while connecting to ERP, response:'));
            $this->_erpLogger->info($this->_queryResponse);
            $this->_erpLogger->info(strtoupper('------------------------------------------------------------------------'));
            if ($type == 'status_bool') {
                return true;
            } else {
                return __("<span style='color: darkred'>Something wrong happened while connecting to ERP, please try again later.</span>");
            }
        }

        if ($type == 'status' || $type == 'status_bool') {
            if ($queryStatusObject->status == 'ok') {
                $color = "green";
            } elseif ($queryStatusObject->status == 'warning') {
                $color = "darkorange";
            } else {
                $color = "darkred";
                if ($type == 'status_bool') {
                    return true;
                }
            }
            if ($type == 'status_bool') {
                return false;
            }

            if (isset($queryStatusObject->output->status)) {
                $htmlResponse = "<span>" . $queryStatusObject->message . "</span><br/>Last reported status: <span style='color: colorReplace'>" . $queryStatusObject->output->status . "</span>";
            } else {
                if($color == 'darkred'){
                    $htmlResponse = "<span>There was a problem while processing the order on the ERP:</span><br/><span style='color: colorReplace'>" . $queryStatusObject->message . "</span><br/> Resend it back to the ERP or contact us.";
                } else {
                    $htmlResponse = "<span style='color: colorReplace'>" . $queryStatusObject->message . "</span>";
                }
            }
            return __(str_replace('colorReplace', $color, $htmlResponse));
        } else {
            if (isset($queryStatusObject->output->$type)) {
                return __("<span>" . $queryStatusObject->output->$type . "</span>");
            } else {
                return '-';
            }
        }
    }

    /**
     * @return \Magento\Framework\Phrase|string
     */
    public function getErpLog()
    {
        if ($this->getOrder()->getErpSyncStatus()) {
            $messages = (Array)json_decode($this->getOrder()->getErpSyncLog());
            $response = "";
            foreach ($messages as $message) {
                if (strpos($message, 'success') !== false || strpos($message, 'Success') !== false) {
                    $response .= __("<span style='color: green'>" . $message . "</span> <br/>");
                } else {
                    $response .= __("<span style='color: darkred'>" . $message . "</span> <br/>");
                }
            }
            return $response;
        } else {
            return __("<span style='color: darkorange'>No logs found...</span>");
        }
    }

    /**
     * @return bool
     */
    public function isErpEnabled()
    {
        return $this->_erpHelper->getEnable() ? true : false;
    }
}