<?php
namespace Qxd\ERP\Helper;

/**
 * Created by PhpStorm.
 * User: jgutierrez@qxdev.com
 * Date: 10/01/19
 * Time: 11:15 AM
 */

use DateTime;

class Data extends \Magento\Framework\App\Helper\AbstractHelper {
    /**
     * API url to send the orders
     */
    const REQUEST_API_URL = 'api/magento2/status-order';

    /**
     * Http API KEY header name
     */
    const ERP_HTTP_KEY = 'ERP-Security-Api-Key';

    /**
     * Http API KEY header name
     */
    const ERP_HTTP_SECRET = 'ERP-Security-Secret';

    /**
     * @var \Qxd\ERP\Logger\ERPLogger
     */
    protected $_erpLogger;

    /**
     * @var \Magento\Framework\Stdlib\DateTime\TimezoneInterface
     */
    protected $_timezone;

    /**
     * @var \Magento\Framework\Encryption\EncryptorInterface
     */
    protected $_encryptor;

    /**
     * @var \Magento\Framework\App\DeploymentConfig
     */
    protected $_deploymentConfig;

    /**
     * Config constants
     */

    const CONFIG_ENABLE = 'erp/general/enable';

    const CONFIG_ORDER_STATUS = 'erp/general/order_status';

    const CONFIG_ORDER_ID = 'erp/general/order_id';

    const CONFIG_API_KEY = 'erp/general/api_key';

    const CONFIG_API_SECRET = 'erp/general/api_secret';

    /**
     * Data constructor.
     * @param \Magento\Framework\App\Helper\Context $context
     * @param \Qxd\ERP\Logger\ERPLogger $erpLogger
     * @param \Magento\Framework\Stdlib\DateTime\TimezoneInterface $timezone
     * @param \Magento\Framework\Encryption\EncryptorInterface $encryptor
     * @param \Magento\Framework\App\DeploymentConfig $deploymentConfig
     */
    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        \Qxd\ERP\Logger\ERPLogger $erpLogger,
        \Magento\Framework\Stdlib\DateTime\TimezoneInterface $timezone,
        \Magento\Framework\Encryption\EncryptorInterface $encryptor,
        \Magento\Framework\App\DeploymentConfig $deploymentConfig,
        \Magento\Sales\Model\ResourceModel\Order\Tax\Item $taxItem,
        \Magento\Framework\App\ResourceConnection $resource
    )
    {
        parent::__construct($context);
        $this->_erpLogger = $erpLogger;
        $this->_timezone = $timezone;
        $this->_encryptor = $encryptor;
        $this->_deploymentConfig = $deploymentConfig;
        $this->_taxItem = $taxItem;
        $this->_resource = $resource;
    }

    /**
     * @return mixed
     */
    public function getEnable() {
        return $this->scopeConfig->getValue(
            self::CONFIG_ENABLE,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }

    /**
     * @return mixed
     */
    public function getOrderStatusConfig() {
        return $this->scopeConfig->getValue(
            self::CONFIG_ORDER_STATUS,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }

    /**
     * @return mixed
     */
    public function getOrderIdConfig() {
        return $this->scopeConfig->getValue(
            self::CONFIG_ORDER_ID,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }

    /**
     * @return mixed
     */
    public function getApiKey() {
        return $this->scopeConfig->getValue(
            self::CONFIG_API_KEY,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }

    /**
     * @return mixed
     */
    public function getApiSecret() {
        $value = $this->scopeConfig->getValue(
            self::CONFIG_API_SECRET,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
        return $this->_encryptor->decrypt($value);
    }

    /**
     * @return mixed
     */
    public function getCleanBaseUrl()
    {
        return str_replace("/", "", str_replace("http://", "", str_replace("https://", "", $this->scopeConfig->getValue("web/unsecure/base_url"))));
    }

    /**
     * @param $parameters
     * @param $apiUrl
     * @return bool|string
     */
    public function sendRequest($parameters, $apiUrl)
    {   

        try {

            $this->_erpLogger->info(strtoupper('Calling ERP endpoint: ' . $apiUrl));
            $erpApiKey = $this->getApiKey();
            $erpApiSecret = $this->getApiSecret();
            $nonce = random_bytes(SODIUM_CRYPTO_SECRETBOX_NONCEBYTES);
            $ciphertext = sodium_crypto_secretbox($this->getCleanBaseUrl(), $nonce, hex2bin($erpApiSecret));
            $secretEncoded = base64_encode($nonce . $ciphertext);
            $fieldsString = http_build_query($parameters);
            $requestApiUrl = $this->_deploymentConfig->get('custom/qxd-erp-request-api');

            if(!$requestApiUrl) {
                throw new \Exception('Not URL API request has been defined.');
            }

            $curl = curl_init();
            curl_setopt_array($curl, array(
                CURLOPT_HTTPHEADER => array(
                    self::ERP_HTTP_KEY . ': ' . $erpApiKey,
                    self::ERP_HTTP_SECRET . ': ' . $secretEncoded,
                ),
                CURLOPT_RETURNTRANSFER => 1,
                CURLOPT_URL => $requestApiUrl . $apiUrl,
                CURLOPT_POST => 1,
                CURLOPT_POSTFIELDS => $fieldsString
            ));

            if(!$responseData = curl_exec($curl)){
                throw new \Exception(curl_error($curl), curl_errno($curl));
            }

            curl_close($curl);
            return $responseData;
        } catch (\Exception $e) {
            $this->_erpLogger->error(strtoupper('There was a problem sending the request, ' . $e->getMessage()));
        }
        return '{}';
    }

    /**
     * @param $order
     * @param $resend
     * @return array
     * @throws \Exception
     */
    public function prepareOrderRequestData($order, $resend)
    {
        $this->_erpLogger->info(strtoupper('      Processing order #' . $order->getIncrementId() . '.'));
        $isCustomerGuest = $order->getCustomerIsGuest();

        if (empty($order->getShippingAddress())) {
            if (empty($order->getBillingAddress())) {
                $this->_erpLogger->info(strtoupper('There are not addresses related to this order, stopping process.'));
                $dateTime = new DateTime("now");
                $order->setErpSyncLog(json_encode(array($dateTime->format('Y-m-d H:i:s') . ': Error: There are not addresses related to this order.')));
                $order->setErpSyncStatus(1);
                $order->save();
                return array();
            }
            $shippingAddress = $order->getBillingAddress()->getData();
        } else {
            $shippingAddress = $order->getShippingAddress()->getData();
        }

        $billingAddress = $order->getBillingAddress()->getData();
        $createAt = $order->getCreatedAt();
        $createAt = $this->_timezone->date(new DateTime($createAt));
        $date = $createAt->format('Y-m-d');
        switch ($order->getPayment()->getMethodInstance()->getCode()) {
            case 'banktransfer':
                $payMethod = '04';
                break;
            case 'cashondelivery':
            case 'free':
                $payMethod = '01';
                break;
            case 'checkmo':
                $payMethod = '03';
                break;
            default:
                $payMethod = '02';
                break;
        }

        $qxdErpId = NULL;
        $qxdErpIdType = NULL;
        if($order->getCustomerId()){
            $qxdErpId = $order->getErpId();
            $qxdErpIdType = $order->getErpIdType();
        }
        $postOrderData = array(
            'store_hash' => $this->getCleanBaseUrl(),
            'resend' => $resend,
            'order_id' => $order->getId(),
            'client' => array(
                'first_name' => $isCustomerGuest ? $shippingAddress['firstname'] : $order->getCustomerFirstName(),
                'last_name' => $isCustomerGuest ? $shippingAddress['lastname'] : $order->getCustomerLastName(),
                'email' => $order->getCustomerEmail(),
                'home_phone' => trim($shippingAddress['telephone']),
                'company' => trim($shippingAddress['company']),
                'address_description' =>
                    $billingAddress['country_id'] . ', ' .
                    $billingAddress['region'] . ', ' .
                    $billingAddress['street'],
                'shipping_address_description' =>
                    $shippingAddress['country_id'] . ', ' .
                    $shippingAddress['region'] . ', ' .
                    $shippingAddress['street'],
                'tipo_identificacion' => $qxdErpIdType ? $qxdErpIdType : '',
                'identifier' => $qxdErpId ? $qxdErpId : '',
                'provincia' => null,
                'canton' => null,
                'distrito' => null,
                'barrio' => null,
                'is_company' => 0,
                'receptor_electronico' => 0,
                'mobile_phone' => '',
                'address' => ''
            ),
            'medio_pago' => $payMethod,
            'date' => $date,
            'description' => 'Order #' . $order->getId(),
            'ship_different_address' => count(array_diff($billingAddress, $shippingAddress)) <= 2 ? 0 : 1,
            'subtotal' => $order->getBaseSubtotalInclTax() - $order->getBaseTaxAmount(),
            'shipping' => $order->getBaseShippingInclTax(),
            'taxes' => $order->getBaseTaxAmount(),
            'total' => $order->getBaseGrandTotal(),
            'discount' => abs($order->getBaseDiscountAmount()),
            'billing_state' => null,
            'billing_city' => null,
            'shipping_state' => null,
            'shipping_city' => null,
            'shipping_provincia' => null,
            'shipping_canton' => null,
            'shipping_distrito' => null,
            'shipping_taxes_excluded' => 0,
            'servicios_gravados' => 0,
            'plazo_credito' => 0,
            'cr_invoice' => 1,
            'discount_is_fixed' => 1,
            'printed_instructions' => '', //*
            'billing_address' => '',
            'shipping_address' => '',
            'special_instructions' => '',
            'condicion_venta' => '01',
            'terminal_id' => '00001',
            'sucursal_id' => '001',
            'codigo_moneda' => $order->getBaseCurrencyCode(),
            'codigo_moneda_base' => $order->getBaseCurrencyCode(),
            'billing_country' => 'CR',
            'shipping_country' => 'CR',
            'naturaleza_descuento' => 'Cupon de descuento',
        );
        $orderItems = array();

        $tax_result = $this->_taxItem->getTaxItemsByOrderId($order->getId());
        $tax_array = [];
        foreach ($tax_result as $tax) {
            $tax_array[$tax["item_id"]] = $tax;
        }

        foreach ($order->getAllItems() as $orderItem) {

            $is_iva = false;
            $iva_code = '';
            if(isset($tax_array[$orderItem->getItemId()])){
                $item_tax = $tax_array[$orderItem->getItemId()];
                if (strpos($item_tax["code"], 'rate_erp_qxd_iva_') !== false){
                    $iva_code = substr($item_tax["code"], -2); 
                    $is_iva = true;
                }
            }
            $taxes_param = ['01'];
            if($is_iva){
                $taxes_param = [['tax_code' => '01', 'tax_rate' => $iva_code]];
            }

            $orderItems[] = array(
                'item_id' => $orderItem->getProductId(),
                'sku' => $orderItem->getSku(),
                'name' => $orderItem->getName(),
                'qty' => $orderItem->getQtyOrdered(),
                'unit_price' => $orderItem->getBasePrice(),
                'precio_unitario' => $orderItem->getBasePrice(),
                'total' => $orderItem->getRowTotalInclTax(),
                'subtotal' => $orderItem->getBaseRowTotal(),
                'desription' => empty($orderItem->getDescription()) ? 'No description.' : $orderItem->getDescription(),
                'discount' => $orderItem->getBaseDiscountAmount(),
                'price_changed' => 0,
                'monto_descuento' => $orderItem->getBaseDiscountAmount(),
                'type_id' => 1,
                'exchange' => 1,
                'taxes' => $taxes_param,
                'exclude_from_taxes' => $orderItem->getBaseTaxAmount() <= 0 ? 1 : 0,
                'measurement_unit' => 'kg'
            );
        }
        $postOrderData['items'] = $orderItems;

        $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/testwhy2log');
        $logger = new \Zend\Log\Logger();
        $logger->addWriter($writer);
        $logger->info(print_r($postOrderData,true));
        return $postOrderData;
    }

}

