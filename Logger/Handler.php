<?php
namespace Qxd\ERP\Logger;


/**
 * Created by PhpStorm.
 * User: jgutierrez@qxdev.com
 * Date: 10/01/19
 * Time: 02:34 PM
 */

use Monolog\Logger;

class Handler extends \Magento\Framework\Logger\Handler\Base
{
    /**
     * Logging level
     * @var int
     */
    protected $loggerType = Logger::INFO;

    /**
     * File name
     * @var string
     */
    protected $fileName = '/var/log/erpLogger.log';

}