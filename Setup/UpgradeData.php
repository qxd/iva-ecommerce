<?php
/**
 * Created by PhpStorm.
 * User: developer
 * Date: 30/01/19
 * Time: 12:17 PM
 */

namespace Qxd\ERP\Setup;

use Magento\Customer\Setup\CustomerSetupFactory;
use Magento\Eav\Setup\EavSetupFactory;
use Magento\Framework\Indexer\IndexerRegistry;
use Magento\Framework\Setup\UpgradeDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;


class UpgradeData implements UpgradeDataInterface
{
    /**
     * @var CustomerSetupFactory
     */
    private $customerSetupFactory;

    /**
     * @var IndexerRegistry
     */
    protected $indexerRegistry;

    /**
     * @var \Magento\Eav\Model\Config
     */
    protected $eavConfig;

    /**
     * @var \Magento\Eav\Model\Setup
     */
    protected $eavSetupFactory;

    /**
     * UpgradeData constructor.
     * @param CustomerSetupFactory $customerSetupFactory
     * @param IndexerRegistry $indexerRegistry
     * @param \Magento\Eav\Model\Config $eavConfig
     * @param EavSetupFactory $eavSetupFactory
     */
    public function __construct(
        CustomerSetupFactory $customerSetupFactory,
        IndexerRegistry $indexerRegistry,
        \Magento\Eav\Model\Config $eavConfig,
        EavSetupFactory $eavSetupFactory,
        \Magento\Tax\Model\ClassModelFactory $taxClass,
        \Magento\Tax\Model\Calculation\RateFactory $taxCalculationRate,
        \Magento\Tax\Model\Calculation\RuleFactory $fixtureTaxRule,
        \Magento\Store\Model\StoreManagerInterface  $storeManager,
        \Magento\Framework\App\State $state
    )
    {
        $this->customerSetupFactory = $customerSetupFactory;
        $this->indexerRegistry = $indexerRegistry;
        $this->eavConfig = $eavConfig;
        $this->eavSetupFactory = $eavSetupFactory;
        $this->taxClass = $taxClass;
        $this->taxCalculationRate = $taxCalculationRate;
        $this->fixtureTaxRule = $fixtureTaxRule;
        $this->storeManager = $storeManager;
        $this->state = $state;
    }

    /**
     * Upgrades data for a module
     *
     * @param ModuleDataSetupInterface $setup
     * @param ModuleContextInterface $context
     * @return void
     */
    public function upgrade(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        if (version_compare($context->getVersion(), '1.0.1', '<')) {

            $customerSetup = $this->customerSetupFactory->create(['setup' => $setup]);

            $customerSetup->addAttribute(
                'customer',
                'qxd_erp_id',
                [
                    'label' => 'Identification',
                    'required' => 0,
                    'visible' => 1,
                    'input' => 'text',
                    'type' => 'static',
                    'system' => 0,
                    'position' => 40,
                    'sort_order' => 40
                ]
            );

            $customerSetup->addAttribute(
                'customer',
                'qxd_erp_id_type',
                [
                    'label' => 'Identification Type',
                    'required' => 0,
                    'visible' => 1,
                    'input' => 'text',
                    'type' => 'static',
                    'system' => 0,
                    'position' => 40,
                    'sort_order' => 40
                ]
            );

            /** @var  EavSetupFactory $eavSetup */

            $eavSetup = $this->eavSetupFactory->create(['setup' => $setup]);
            $typeId = $eavSetup->getEntityTypeId('customer');

            $qxdErpId = $eavSetup->getAttribute($typeId, 'qxd_erp_id');
            $customerSetup->getSetup()->getConnection()->insertMultiple(
                $customerSetup->getSetup()->getTable('customer_form_attribute'),
                array(
                    'form_code' => 'customer_account_edit',
                    'attribute_id' => $qxdErpId['attribute_id']
                )
            );

            $qxdErpIdType = $eavSetup->getAttribute($typeId, 'qxd_erp_id_type');
            $customerSetup->getSetup()->getConnection()->insertMultiple(
                $customerSetup->getSetup()->getTable('customer_form_attribute'),
                array(
                    'form_code' => 'customer_account_edit',
                    'attribute_id' => $qxdErpIdType['attribute_id']
                )
            );

            $setup->endSetup();
        }

        if ( version_compare($context->getVersion(), '1.0.3', '<' )) {

            $this->state->setAreaCode('frontend');
            $iva_taxes = [
                ['code' => 'erp_qxd_iva_01', 'rate' => 0, 'title' => 'IVA - Tarifa 0 (Exento)'],
                ['code' => 'erp_qxd_iva_02', 'rate' => 1, 'title' => 'IVA - Tarifa reducida 1%'],
                ['code' => 'erp_qxd_iva_03', 'rate' => 2, 'title' => 'IVA - Tarifa reducida 2%'],
                ['code' => 'erp_qxd_iva_04', 'rate' => 4, 'title' => 'IVA - Tarifa reducida 4%'],
                ['code' => 'erp_qxd_iva_05', 'rate' => 0, 'title' => 'IVA - Transitorio 0%'],
                ['code' => 'erp_qxd_iva_06', 'rate' => 4, 'title' => 'IVA - Transitorio 4%'],
                ['code' => 'erp_qxd_iva_07', 'rate' => 8, 'title' => 'IVA - Transitorio 8%'],
                ['code' => 'erp_qxd_iva_08', 'rate' => 13, 'title' => 'IVA - Tarifa general 13%']
            ];

            foreach ($iva_taxes as $tax) {
                
                $taxClass = $this->taxClass->create();
                $taxClass->setClassName($tax['title']);
                $taxClass->setClassType(\Magento\Tax\Model\ClassModel::TAX_CLASS_TYPE_PRODUCT);
                $taxClass->save();

                $taxCalculationRate = $this->taxCalculationRate->create();
                $taxCalculationRate->setCode('rate_'.$tax['code']);
                $taxCalculationRate->setTaxCountryId("CR");
                $taxCalculationRate->setTaxRegionId("REGION");
                $taxCalculationRate->setZipIsRange("0");
                $taxCalculationRate->setTaxPostcode("*");
                
                //for each de cada store view
                $storeManagerDataList = $this->storeManager->getStores();
                $array_titles =[];
                foreach ($storeManagerDataList as $value) {
                    $array_titles[$value->getStoreId()] = "IVA ".$tax['rate']."%";  
                }
                $taxCalculationRate->setTitle($array_titles);
                $taxCalculationRate->setRate($tax['rate']);

                $taxCalculationRate->save();  
                 
                 
                $fixtureTaxRule = $this->fixtureTaxRule->create();
                $fixtureTaxRule->setCode($tax['code']);
                $fixtureTaxRule->setPriority(0);
                $fixtureTaxRule->setCustomerTaxClassIds(array(3));
                $fixtureTaxRule->setProductTaxClassIds(array($taxClass->getId()));
                $fixtureTaxRule->setTaxRateIds(array($taxCalculationRate->getId()));
                $fixtureTaxRule->save();

            }
        }
    }
}