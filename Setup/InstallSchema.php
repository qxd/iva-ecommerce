<?php
namespace Qxd\ERP\Setup;

/**
 * Created by PhpStorm.
 * User: jgutierrez@qxdev.com
 * Date: 09/01/19
 * Time: 11:29 AM
 */

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

class InstallSchema implements InstallSchemaInterface
{
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $installer = $setup;
        $installer->startSetup();
        $connection = $installer->getConnection();

        if ($connection->tableColumnExists('sales_order_grid', 'erp_sync_status') === false) {
            $connection
                ->addColumn(
                    $setup->getTable('sales_order_grid'),
                    'erp_sync_status',
                    [
                        'type' => \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                        'length' => 0,
                        'comment' => 'QXD - ERP Synchronization Status (Sent/Waiting)',
                        'default' => 0
                    ]
                );
        }

        if ($connection->tableColumnExists('sales_order', 'erp_sync_status') === false) {
            $connection
                ->addColumn(
                    $setup->getTable('sales_order'),
                    'erp_sync_status',
                    [
                        'type' => \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                        'length' => 0,
                        'comment' => 'QXD - ERP Synchronization Status (Sent/Waiting)',
                        'default' => 0
                    ]
                );
        }

        if ($connection->tableColumnExists('sales_order', 'erp_sync_log') === false) {
            $connection
                ->addColumn(
                    $setup->getTable('sales_order'),
                    'erp_sync_log',
                    [
                        'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                        'comment' => 'QXD - ERP Synchronization Log'
                    ]
                );
        }

        $connection->delete('ui_bookmark', 'namespace = "sales_order_grid"');

        $installer->endSetup();
    }
}