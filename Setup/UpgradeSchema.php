<?php
namespace Qxd\ERP\Setup;

/**
 * Created by PhpStorm.
 * User: jgutierrez@qxdev.com
 * Date: 09/01/19
 * Time: 11:29 AM
 */

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\Setup\UpgradeSchemaInterface;

class UpgradeSchema implements UpgradeSchemaInterface
{
    public function upgrade(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $installer = $setup;
        $installer->startSetup();
        $connection = $installer->getConnection();

        if (version_compare($context->getVersion(), '1.0.3', '<' )) {
            if ($connection->tableColumnExists('customer_entity', 'qxd_erp_id') === false) {
                $connection
                    ->addColumn(
                        $setup->getTable('customer_entity'),
                        'qxd_erp_id',
                        [
                            'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                            'comment' => 'QXD - ERP Customer Id',
                        ]
                    );
            }

            if ($connection->tableColumnExists('customer_entity', 'qxd_erp_id_type') === false) {
                $connection
                    ->addColumn(
                        $setup->getTable('customer_entity'),
                        'qxd_erp_id_type',
                        [
                            'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                            'comment' => 'QXD - ERP Customer Id Type',
                        ]
                    );
            }
        }

        if (version_compare($context->getVersion(), '1.0.3', '<' )) {
            if ($connection->tableColumnExists('sales_order', 'erp_id') === false) {
                $connection
                    ->addColumn(
                        $setup->getTable('sales_order'),
                        'erp_id',
                        [
                            'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                            'comment' => 'QXD - ERP Customer Id'
                        ]
                    );
            }

            if ($connection->tableColumnExists('sales_order', 'erp_id_type') === false) {
                $connection
                    ->addColumn(
                        $setup->getTable('sales_order'),
                        'erp_id_type',
                        [
                            'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                            'comment' => 'QXD - ERP Customer Id Type'
                        ]
                    );
            }
        }

        $installer->endSetup();
    }
}