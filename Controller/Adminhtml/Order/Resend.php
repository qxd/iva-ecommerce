<?php

namespace Qxd\ERP\Controller\Adminhtml\Order;

/**
 * Created by PhpStorm.
 * User: jgutierrez@qxdev.com
 * Date: 10/01/19
 * Time: 09:59 AM
 */

use DateTime;
use Magento\Framework\Controller\ResultFactory;

class Resend extends \Magento\Backend\App\Action
{
    /**
     * API url to send the orders
     */
    const REQUEST_API_URL = 'api/magento2/queue-orders';

    /**
     * @var \Qxd\ERP\Logger\ERPLogger
     */
    protected $_logger;

    /**
     * @var \Qxd\ERP\Helper\Data
     */
    protected $_erpHelper;

    /**
     * @var \Magento\Sales\Api\OrderRepositoryInterface
     */
    protected $_orderRepository;

    /**
     * @var \Magento\Framework\Stdlib\DateTime\TimezoneInterface
     */
    protected $_timezone;

    /**
     * Resend constructor.
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Qxd\ERP\Logger\ERPLogger $logger
     * @param \Qxd\ERP\Helper\Data $erpHelper
     * @param \Magento\Sales\Api\OrderRepositoryInterface $orderRepository
     * @param \Magento\Framework\Stdlib\DateTime\TimezoneInterface $timezone
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Qxd\ERP\Logger\ERPLogger $logger,
        \Qxd\ERP\Helper\Data $erpHelper,
        \Magento\Sales\Api\OrderRepositoryInterface $orderRepository,
        \Magento\Framework\Stdlib\DateTime\TimezoneInterface $timezone
    )
    {
        $this->_logger = $logger;
        $this->_timezone = $timezone;
        $this->_erpHelper = $erpHelper;
        $this->_orderRepository = $orderRepository;
        parent::__construct($context);
    }

    /**
     * @return \Magento\Framework\App\ResponseInterface|\Magento\Framework\Controller\ResultInterface
     * @throws \Exception
     */
    public function execute()
    {
        $resultJson = $this->resultFactory->create(ResultFactory::TYPE_JSON);
        $this->_logger->info(strtoupper('------------------------------------------------------------------------'));
        $this->_logger->info(strtoupper('Running QXD - ERP resend.'));

        if ($this->_erpHelper->getEnable()) {

            $this->_logger->info(strtoupper('Starting ERP Order # '  . $this->_request->getParam('order_id') . ' synchronization.'));

            $this->_logger->info(strtoupper('Preparing request data object.'));

            $order = $this->_orderRepository->get($this->_request->getParam('order_id'));

            if(!$order->getErpSyncStatus()) {
                $this->_logger->info(strtoupper("This order hasn't being processed automatically."));
                $this->_logger->info(strtoupper('------------------------------------------------------------------------'));
                $resultJson->setData(array(
                    'status' =>  "This order hasn't being processed automatically."
                ));
                return $resultJson;
            }

            if($order->getStatus() != $this->_erpHelper->getOrderStatusConfig()) {
                $this->_logger->info(strtoupper("This order doesn't meet the configured order status."));
                $this->_logger->info(strtoupper('------------------------------------------------------------------------'));
                $resultJson->setData(array(
                    'status' =>  "This order doesn't meet the configured order status."
                ));
                return $resultJson;
            }

            $this->_logger->info(strtoupper('Sending request to ERP API.'));

            if(!empty($orderRequestArray = $this->_erpHelper->prepareOrderRequestData($order,1))){
                $responseData = $this->_erpHelper->sendRequest(array($orderRequestArray), self::REQUEST_API_URL);
            } else {
                $this->_logger->info(strtoupper('------------------------------------------------------------------------'));
                $resultJson->setData(array(
                    'status' => 'There is a problem with the order data.'
                ));
                return $resultJson;
            }

            $responseObject = (Object) json_decode($responseData);
            if (isset($responseObject->message)) {
                $this->_logger->info(strtoupper($responseObject->message));
            } else {
                $this->_logger->error(strtoupper('Something wrong happened, not seeing response message.'));
                $this->_logger->info(strtoupper('------------------------------------------------------------------------'));
                $resultJson->setData(array(
                    'status' => 'Something wrong happened while connecting to ERP.'
                ));
                return $resultJson;
            }

            try {
                $this->_logger->info(strtoupper('Checking response and updating order.'));
                $this->_logger->info(strtoupper($responseObject->output->{$order->getId()}));
                $erpSyncLog = (Array)json_decode($order->getErpSyncLog());
                $dateTime = new DateTime("now");
                $erpSyncLog[] = $dateTime->format('Y-m-d H:i:s') . ': ' . $responseObject->output->{$order->getId()};
                $order->setErpSyncLog(json_encode($erpSyncLog));
                $order->save();
            } catch (\Exception $e) {
                $this->_logger->error(strtoupper('There was a problem processing the order, ' . $e->getMessage()));
                $resultJson->setData(array(
                    'status' => 'There was a problem processing the order, ' . $e->getMessage()
                ));
                return $resultJson;
            }

            $resultJson->setData(array(
                'status' =>  $responseObject->output->{$order->getId()}
            ));

        } else {
            $this->_logger->info(strtoupper('QXD - ERP is disabled.'));
            $this->_logger->info(strtoupper('------------------------------------------------------------------------'));
            $resultJson->setData(array(
                'status' => 'QXD - ERP extension is disabled.'
            ));
        }
        return $resultJson;
    }
}