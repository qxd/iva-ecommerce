<?php
/**
 * Created by PhpStorm.
 * User: developer
 * Date: 31/01/19
 * Time: 09:57 AM
 */

namespace Qxd\ERP\Plugin\Sales\Controller\Adminhtml\Order\Invoice;


class SavePlugin
{
    /**
     * @var \Magento\Framework\Controller\Result\RedirectFactory
     */
    protected $resultRedirectFactory;

    /**
     * @var \Magento\Framework\Data\Form\FormKey\Validator
     */
    protected $_formKeyValidator;

    /**
     * @var \Magento\Framework\Message\ManagerInterface
     */
    protected $messageManager;

    /**
     * @var \Magento\Sales\Api\OrderRepositoryInterface
     */
    protected $_orderRepository;

    /**
     * SavePlugin constructor.
     * @param \Magento\Framework\Message\ManagerInterface $messageManager
     * @param \Magento\Framework\Controller\Result\RedirectFactory $resultRedirectFactory
     * @param \Magento\Framework\Data\Form\FormKey\Validator $validator
     * @param \Magento\Sales\Api\OrderRepositoryInterface $orderRepository
     */
    public function __construct(
        \Magento\Framework\Message\ManagerInterface $messageManager,
        \Magento\Framework\Controller\Result\RedirectFactory $resultRedirectFactory,
        \Magento\Framework\Data\Form\FormKey\Validator $validator,
        \Magento\Sales\Api\OrderRepositoryInterface $orderRepository
    )
    {
        $this->resultRedirectFactory = $resultRedirectFactory;
        $this->_formKeyValidator = $validator;
        $this->messageManager = $messageManager;
        $this->_orderRepository = $orderRepository;
    }

    /**
     * @param \Magento\Sales\Controller\Adminhtml\Order\Invoice\Save $subject
     * @param $response
     * @return mixed
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function afterExecute(
        \Magento\Sales\Controller\Adminhtml\Order\Invoice\Save $subject,
        $response
    )
    {
        $orderId = $subject->getRequest()->getParam('order_id');
        $order = $this->_orderRepository->get($orderId);
        if (!empty($subject->getRequest()->getParam('qxd_erp_id')) && !empty($subject->getRequest()->getParam('qxd_erp_id_type'))) {
            try {
                $order->setErpId($subject->getRequest()->getParam('qxd_erp_id'));
                $order->setErpIdType($subject->getRequest()->getParam('qxd_erp_id_type'));
                $this->_orderRepository->save($order);
            } catch (\Exception $e) {
                throw new \Magento\Framework\Exception\LocalizedException(
                    __('Something wrong happened updating the custom id attributes.')
                );
            }
        }
        return $response;
    }
}